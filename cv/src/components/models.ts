export interface Certification {
  name: string;
  issuer: string;
  url: string;
  certificationUrl: string;
  certificationId: string;
  certificationDate: string;
  picture: string;
  skillDomains: Array<string>;
}

export interface Experience {
  job_name: string;
  society: string;
  website: string;
  mission: string;
  tasks: Array<string>;
  location: string;
  length: string;
  date_start: string;
  date_end: string;
  skills: Array<string>;
}

export interface Project {
  title: string;
  description: string;
  website: string;
  git: string;
  owned: boolean;
  tags: Array<string>;
  badges: Array<string>;
}

export interface Link {
  icon: string;
  color: string;
  description: string;
  website: string;
  badges: Array<string>;
}

export interface Skill {
  title: string;
  skills: Array<string>;
}

export interface Training {
  title: string;
  year: string;
  location: string;
  description: string;
}

export interface Publication {
  title: string;
  date: string;
  publisher: string;
  lang: string,
  description: string;
  link: string;
  tags: Array<string>,
}
