export default [
  'Hello and be welcomed !',
  "I'm Yann Ponzoni, a french software engineer.",
  "Fan of technologies, I've start to learn programing by myself with PHP at the age of 16 (in the '90s), and validated" +
    ' a Master Degree (paths "Advanced technologies of web" and "Software engineering") in 2009.',
  "I've worked in several companies, in different sectors (bank, insurance, industry, ...), and in different roles " +
    '(developer, architect, team leader, ...).',
  'My experiences over those years made me discover a huge number of languages (PHP, Golang, C, C++, C#, Java, HTML 5, ' +
    'Javascript, Typescript, Bash, Python, SQL....) and tons of technologies (Git, GitHub, GitLab, MySQL, Postgresql, ' +
    'MongoDb, Redis, ELK, GSA, Chef, Puppet, 0MQ, Apache2, Nginx, Docker, Kubernetes...).',
  'I do a permanent technological watch and work on numerous personal projects, to apply the new things I discover each day.',
  'Lastly, I choose to use my skills by focusing on the quality and security aspect of the projects. I fight against ' +
    'the technical debts with a DevOps philosophy, by using Continuous Integration and Continuous Delivery (CI/CD). ' +
    'I also use my architect skills to help new projects to start with the best chances to success.',
  "I'm agile (Scrum / Kanban) and can work in full remote or in Montpellier area (France).",
];
