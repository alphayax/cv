export default [
  {
    name: 'Devops Leader (DOL)',
    issuer: 'Devops Institute',
    url: 'https://www.devopsinstitute.com/certifications/devops-leader/',
    certificationId: '21874051',
    certificationDate: '2021-02-12',
    picture: '/certif-icons/devops-leader.png',
    skillDomains: ['devops']
  },
  {
    name: 'KUBE-ORCH',
    issuer: 'M2i Formation',
    url: 'https://www.m2iformation.fr/formation-kubernetes-orchestrer-ses-conteneurs/KUB-ORCH/',
    certificationId: '22110051',
    certificationDate: '2022-11-20',
    skillDomains: ['k8s']
  },
  {
    name: 'Google Cloud - Associate Cloud Engineer (ACE)',
    issuer: 'Google Cloud',
    url: 'https://cloud.google.com/learn/certification/cloud-engineer',
    certificationUrl: 'https://google.accredible.com/95e69957-a879-4047-a32c-7709e9e24acb?key=3e0c7ce16240ac5425b1cb5a45da909bf4d5219ab98be3c0fa5a930f18b8498d',
    certificationId: 'IVakNI',
    certificationDate: '2023-05-13',
    picture: '/certif-icons/gcp-ace.png',
    skillDomains: ['gcp', 'k8s']
  },
  {
    name: 'Certified Stormshield Network Administrator (CSNA)',
    issuer: 'Stormshield',
    url: 'https://www.stormshield.com/our-support/services/training/sns/csna-training-course/',
    certificationId: '02V43CSNA43536',
    certificationDate: '2023-07-19',
    picture: '/certif-icons/csna.jpg',
    skillDomains: ['firewall']
  },
  {
    name: 'CKAD - Certified Kubernetes Application Developer',
    issuer: 'The Linux Foundation',
    url: 'https://training.linuxfoundation.org/certification/certified-kubernetes-application-developer-ckad/',
    certificationUrl: 'https://www.credly.com/badges/c21cab42-e2ea-42ce-aa20-a762f8d4a9e6/public_url',
    certificationId: 'LF-vbjst3c5hk',
    certificationDate: '2023-09-18',
    picture: '/certif-icons/ckad.png',
    skillDomains: ['k8s']
  },
  {
    name: 'CKA - Certified Kubernetes Administrator',
    issuer: 'The Linux Foundation',
    url: 'https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/',
    certificationUrl: 'https://www.credly.com/badges/48c2f4a5-8cd6-4270-a5af-532b546edaaf/public_url',
    certificationId: 'LF-ns0fs7vgqf',
    certificationDate: '2023-10-01',
    picture: '/certif-icons/cka.png',
    skillDomains: ['k8s']
  },
  {
    name: 'TeamOps Foundations',
    issuer: 'Gitlab',
    url: 'https://levelup.gitlab.com/courses/teamops',
    certificationUrl: 'https://levelup.gitlab.com/c/nUYGmD3rQJ-ldlejBUYxsQ',
    certificationId: 'nUYGmD3rQJ-ldlejBUYxsQ',
    certificationDate: '2023-10-18',
    picture: '/certif-icons/gitlab-teamops.png',
    skillDomains: ['devops']
  },
  {
    name: 'AWS - Certified Cloud Practitioner',
    issuer: 'Amazon Web Services',
    url: 'https://aws.amazon.com/fr/certification/certified-cloud-practitioner/',
    certificationUrl: 'https://www.credly.com/badges/16419f97-69eb-4d34-8d7f-748524675791/public_url',
    certificationId: '79QLXMNC0BF4QZWJ',
    certificationDate: '2023-10-20',
    picture: '/certif-icons/aws-cloudPractitioner.png',
    skillDomains: ['aws']
  },
  {
    name: 'IBM - Blockchain essentials',
    issuer: 'IBM',
    url: '',
    certificationUrl: 'https://www.credly.com/badges/c21cab42-e2ea-42ce-aa20-a762f8d4a9e6/public_url',
    certificationId: '',
    certificationDate: '2018-06-06',
    picture: '/certif-icons/ibm-Blockchain_Essentials.png',
    skillDomains: ['blockchain']
  },
  {
    name: 'Kubernetes and IBM Cloud Container Service',
    issuer: 'IBM',
    url: '',
    certificationUrl: 'https://www.credly.com/badges/e072fa40-e10b-45a7-a221-11bed92afb87/public_url',
    certificationId: '',
    certificationDate: '2018-06-06',
    picture: '/certif-icons/ibm-Cloud_Conta_Serv_K8s.png',
    skillDomains: ['k8s']
  },
];
