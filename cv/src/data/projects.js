export default [
  {
    title: 'Tarota',
    description: 'A solution to save and follow scores of Tarot games',
    owned: true,
    website: 'https://www.tarota.alphayax.com',
    git: 'https://gitlab.com/tarota',
    tags: ['Golang', 'Docker', 'Kubernetes', 'VueJS', 'Vuetify'],
  },
  {
    title: 'Domitia',
    description: 'IoT Hub organized with microservices to handle Zigbee devices (MQTT), ' +
      'Ewelink devices and SmartThings devices (Samsung). It also provides a web interface to ' +
      'manage devices and a REST API to interact with them. Communication between microservices ' +
      'is done with GRPC',
    owned: true,
    git: 'https://gitlab.com/domitia',
    tags: ['Golang', 'GRPC', 'MQTT', 'IoT', 'Docker', 'Kubernetes', 'Microservices', 'VueJS', 'Vuetify'],
  },
  {
    title: 'Torrent Operator',
    description: 'A Kubernetes operator to add, delete and manage torrents via a Kubernetes Cluster. Works with ' +
      'Transmission and qBittorrent',
    owned: true,
    git: 'https://github.com/alphayax/torrent-operator',
    tags: ['Golang', 'Kubernetes', 'Kubebuilder', 'Operator', 'Transmission', 'qBittorrent', 'CRD'],
    badges: [
      'https://img.shields.io/github/stars/alphayax/torrent-operator?style=plastic',
      'https://img.shields.io/github/forks/alphayax/torrent-operator?style=plastic',
      'https://img.shields.io/github/license/alphayax/torrent-operator?style=plastic'
    ],
  },
  {
    title: 'Microservices demo',
    description: 'A demo of microservices architecture with Kubernetes',
    owned: true,
    git: "https://github.com/alphayax/microservices-demo",
    tags: ['Golang', 'Docker', 'Kubernetes', 'Microservices'],
    badges: [
      'https://img.shields.io/github/stars/alphayax/microservices-demo?style=plastic',
      'https://img.shields.io/github/forks/alphayax/microservices-demo?style=plastic',
      'https://img.shields.io/github/license/alphayax/microservices-demo?style=plastic'
    ],
  },
  {
    title: 'Renum',
    description: 'A solution to rename by re-numbering files in directories',
    owned: true,
    git: "https://github.com/alphayax/renum",
    tags: ['Golang'],
    badges: [
      'https://img.shields.io/github/stars/alphayax/renum?style=plastic',
      'https://img.shields.io/github/forks/alphayax/renum?style=plastic',
      'https://img.shields.io/github/license/alphayax/renum?style=plastic'
    ],
  },
  {
    title: 'API PHP Freebox',
    description: 'PHP SDK to interact with the Freebox',
    git: 'https://github.com/alphayax/freebox_api_php',
    owned: true,
    tags: ['PHP', 'SDK', 'Freebox'],
    badges: [
      'https://img.shields.io/github/stars/alphayax/freebox_api_php?style=plastic',
      'https://img.shields.io/github/forks/alphayax/freebox_api_php?style=plastic',
      'https://img.shields.io/github/license/alphayax/freebox_api_php?style=plastic'
    ],
  },
  {
    title: 'Freehub',
    description:
      'A solution (backend + frontend) to interact with your freebox. Display information about movies stored on NAS',
    git: 'https://gitlab.com/alphayax/freehub',
    tags: ['PHP', 'Freebox', 'Angular JS', 'Docker', 'Kubernetes'],
    owned: true,
    badges: [
      'https://img.shields.io/gitlab/stars/alphayax/freehub?style=plastic',
      'https://img.shields.io/gitlab/forks/alphayax/freehub?style=plastic',
      'https://img.shields.io/gitlab/license/alphayax/freehub?style=plastic'
    ],
  },
  {
    title: 'Curriculum Vitae',
    description: 'My Curriculum Vitae',
    website: 'https://yann-ponzoni.eu',
    git: 'https://gitlab.com/alphayax/cv',
    owned: true,
    tags: ['VueJS', 'Quasar', 'Docker', 'Kubernetes'],
    badges: [
      'https://img.shields.io/gitlab/stars/alphayax/cv?style=plastic',
      'https://img.shields.io/gitlab/forks/alphayax/cv?style=plastic',
      'https://img.shields.io/gitlab/license/alphayax/cv?style=plastic&color=orange'
    ],
  },
  {
    title: 'Crontab generator',
    description: 'An online tool to generate scheduled tasks with Cron',
    website: 'https://crontab.alabasta.alphayax.com',
    git: 'https://gitlab.com/alphayax/crontab.alphayax.com',
    owned: true,
    tags: ['Angular JS', 'Docker', 'Kubernetes'],
    badges: [
      'https://img.shields.io/gitlab/stars/alphayax/crontab?style=plastic',
      'https://img.shields.io/gitlab/forks/alphayax/crontab?style=plastic',
      'https://img.shields.io/gitlab/license/alphayax/crontab?style=plastic&color=green'
    ]
  },
  {
    title: 'AppScan SAST Action',
    description: 'A github action to run AppScan (SAST) on your project',
    git: 'https://github.com/alphayax/appscan-sast-action',
    tags: ['Github Action', 'Docker', 'AppScan'],
    owned: true,
    badges: [
      'https://img.shields.io/github/stars/alphayax/appscan-sast-action?style=plastic',
      'https://img.shields.io/github/forks/alphayax/appscan-sast-action?style=plastic',
      'https://img.shields.io/github/license/alphayax/appscan-sast-action?style=plastic'
    ]
  },
  {
    title: 'Helm charts',
    description: 'A collection of helm charts proudly made by me',
    git: 'https://gitlab.com/alphayax/helm-charts',
    tags: ['Helm', 'Kubernetes'],
    owned: true,
    badges: [
      'https://img.shields.io/gitlab/stars/alphayax/helm-charts?style=plastic',
      'https://img.shields.io/gitlab/forks/alphayax/helm-charts?style=plastic',
      'https://img.shields.io/gitlab/license/alphayax/helm-charts?style=plastic&color=green'
    ]
  },
  {
    title: 'OpenCloud',
    description: 'Open Cloud is an open source software letting you intereact ' +
      'with your trusted partners over a secure plateform. It helps you to ' +
      'share data, compute, storage and algorithmic services.',
    website: 'https://o-cloud.io/',
    git: 'https://gitlab.com/o-cloud',
    tags: ['Golang', 'Blockchain'],
    owned: false,
  },
  {
    title: 'Go Binance',
    description: 'SDK to interact with the Binance API',
    git: 'https://github.com/adshao/go-binance',
    tags: ['Golang', 'Binance'],
    owned: false,
  },
  {
    title: 'Plex Media Server - Docker',
    description: 'Official Docker container for Plex Media Server',
    git: 'https://github.com/plexinc/pms-docker',
    tags: ['Docker', 'Helm', 'Kubernetes'],
    owned: false,
  },
  {
    title: 'Kubernetes website',
    description: 'Kubernetes documentation',
    website: 'https://kubernetes.io/',
    git: 'https://github.com/kubernetes/website',
    tags: ['Kubernetes'],
    owned: false,
  },
  {
    title: 'MicroK8s core addons',
    description: 'Canonical MicroK8s core addons',
    website: 'https://microk8s.io/docs/addons',
    git: 'https://github.com/canonical/microk8s-core-addons',
    tags: ['Kubernetes', 'MicroK8s', 'Ingress Nginx'],
    owned: false,
  },
  {
    title: 'Bitnami Helm Charts',
    description: 'Helm charts maintained by Bitnami for Kubernetes applications',
    website: 'https://bitnami.com/',
    git: 'https://github.com/bitnami/charts',
    tags: ['Kubernetes', 'Helm'],
    owned: false,
  },
];
