export default [
  {
    title: 'microK8s: Le Kube facile',
    date: '2023-10-19',
    publisher: 'WeScale blog',
    lang: 'fr',
    description: 'An article about MicroK8s, a lightweight Kubernetes distribution',
    link: 'https://blog.wescale.fr/microk8s-le-kube-facile',
    tags: ['Kubernetes'],
  },
  {
    title: 'Ségrégation des flux au sein d\'un cluster Kubernetes',
    date: '2024-07-23',
    publisher: 'WeScale blog',
    lang: 'fr',
    description: 'An article about network policies in Kubernetes',
    link: 'https://blog.wescale.fr/s%C3%A9gr%C3%A9gation-des-flux-r%C3%A9seaux-au-sein-dun-cluster-kubernetes',
    tags: ['Kubernetes', 'Network policies', 'Kyverno'],
  }
];
