export default [
  {
    title: 'Bac Scientifique',
    year: 2002,
    location: 'Lycée Georges Clémenceau - Montpellier',
  },
  {
    title: 'BTS Informatique de Gestion',
    year: 2004,
    location: 'Lycée Jean Mermoz - Montpellier',
  },
  {
    title: 'IUP Génie Mathématique et Informatique',
    year: 2007,
    location: 'Université Montpellier II - Montpellier',
  },
  {
    title: 'License Informatique',
    year: 2006,
    location: 'Université Montpellier II - Montpellier',
  },
  {
    title:
      "Diplome d'Etudes Universitaire Général (DEUG) Mathématique et Informatique",
    year: 2005,
    location: 'Université Montpellier II - Montpellier',
  },
  {
    title: 'Master II Informatique',
    year: 2008,
    location: 'Université Montpellier II - Montpellier',
    description:
      'Specialities: Advanced web technologies, Artificial Intelligence, Software Engineering',
  },
];
