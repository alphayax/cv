export default [
  {
    icon: 'mdi-linkedin',
    color: 'blue',
    description: 'LinkedIn profile',
    website: 'https://www.linkedin.com/in/yannponzoni',
  },
  {
    icon: 'mdi-gitlab',
    color: 'deep-orange',
    description: 'GitLab profile',
    website: 'https://gitlab.com/alphayax',
  },
  {
    icon: 'mdi-github',
    color: 'white',
    description: 'GitHub profile',
    website: 'https://github.com/alphayax',
    badges: [
      'https://img.shields.io/github/followers/alphayax',
      'https://img.shields.io/github/stars/alphayax',
    ]
  },
  {
    icon: 'mdi-stack-overflow',
    color: 'orange',
    description: 'StackOverflow profile',
    website: 'https://stackoverflow.com/users/1517814/alphayax',
    badges: [
      'https://img.shields.io/stackexchange/stackoverflow/r/1517814',
    ],
  },
  {
    icon: 'mdi-docker',
    color: 'blue',
    description: 'DockerHub profile',
    website: 'https://hub.docker.com/u/alphayax',
  }
];
